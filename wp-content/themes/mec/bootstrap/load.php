<?php
/**
 * Enqueue scripts and stylesheet
 */
add_action('wp_enqueue_scripts', 'theme_enqueue_scripts');
add_action('wp_enqueue_scripts', 'theme_enqueue_style');

/**
 * Theme support
 */
add_theme_support('post-thumbnails');

function theme_enqueue_style()
{
    wp_enqueue_style(
        'template-style',
        asset('app.css'),
        false
    );
}

function theme_enqueue_scripts()
{
    wp_enqueue_script(
        'template-scripts',
        asset('app.js'),
        'jquery',
        '1.0',
        true
    );

    $protocol = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';

    $params = array(
       'ajax_url' => admin_url('admin-ajax.php', $protocol)
    );

    wp_localize_script('template-scripts', 'ajax_obj', $params);
}

if (!function_exists('themeSetup')) {
    /**
     * setup support for theme
     *
     * @return void
     */
    function themeSetup()
    {
        // Register menus
        register_nav_menus( array(
    		'main-menu' => __('Main Menu', 'vicoders')
    	) );

    }

    add_action('after_setup_theme', 'themeSetup');
}

if (!function_exists('themeSidebars')) {
    /**
     * register sidebar for theme
     *
     * @return void
     */
    function themeSidebars()
    {
        $sidebars = [
            // [
            //     'name'          => __('Sidebar', 'vicoders'),
            //     'id'            => 'main-sidebar',
            //     'description'   => __('Main Sidebar', 'vicoders'),
            //     'before_widget' => '<section id="%1$s" class="widget %2$s">',
            //     'after_widget'  => '</section>',
            //     'before_title'  => '<h2 class="widget-title">',
            //     'after_title'   => '</h2>',
            // ],

            [
                'name'          => __('Footer Địa Chỉ', 'vicoders'),
                'id'            => 'footer_diachi',
                'description'   => __('Main Sidebar', 'vicoders'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],

            [
                'name'          => __('Footer Giới Thiệu', 'vicoders'),
                'id'            => 'footer_about',
                'description'   => __('Main Sidebar', 'vicoders'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],

            [
                'name'          => __('Footer MXH', 'vicoders'),
                'id'            => 'footer_share',
                'description'   => __('Main Sidebar', 'vicoders'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],

            [
                'name'          => __('Footer Hình Thức Thanh Toán', 'vicoders'),
                'id'            => 'footer_thanhtoan',
                'description'   => __('Main Sidebar', 'vicoders'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],

            [
                'name'          => __('Header', 'vicoders'),
                'id'            => 'header',
                'description'   => __('Main Sidebar', 'vicoders'),
                'before_widget' => '<div id="%1$s" class="widget %2$s contact_header col-md-8">',
                'after_widget'  => '</div>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
        ];

        foreach ($sidebars as $sidebar) {
            register_sidebar($sidebar);
        }
    }

    add_action('widgets_init', 'themeSidebars');
}


if (!function_exists('getLinkForPreview')) {
    function getLinkForPreview($post_id)
    {
        $files = get_package_data($post_id, 'files');
        if (!is_array($files)) {
            return "";
        }

        $ind = -1;
        foreach ($files as $i => $sfile) {
            $ifile = $sfile;
            $sfile = explode(".", $sfile);
            if (in_array(end($sfile), array('pdf', 'doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx'))) {
                $ind = \WPDM_Crypt::Encrypt($ifile);
                break;
            }
        }

        if ($ind == -1) {
            return "";
        }

        // $url = wpdm_download_url($post_id, 'ind=' . $ind);
        // $permalink   = get_permalink($post_id);
        // $permalink   = urlencode($permalink);
        // $sap         = strpos($permalink, '?') ? '&' : '?';
        // $url_preview = urlencode($permalink . $sap . "wpdmdl={$post_id}&ind={$ind}");
        
        $permalink   = get_permalink($post_id);
        // $permalink   = urlencode($permalink);
        $sap         = strpos($permalink, '?') ? '&' : '?';
        $url_preview = $permalink . $sap . "wpdmdl={$post_id}&ind={$ind}";
        if (strpos($ifile, "://")) {
            $url_preview = $ifile;
        }
;       return $url_preview;
    }
}


function SearchFilter($query)   
{  
    if ($query->is_search)   
    {  
        $query->set('post_type', array('wpdmpro', 'news'));  
    }  
    return $query;  
}  
add_filter('pre_get_posts', 'SearchFilter');


// custom logo 
class setting {
    function __construct() {
        add_theme_support( 'custom-logo' );
    }
}
new setting();

function wpdocs_custom_excerpt_length( $length ) {
    return 30;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );