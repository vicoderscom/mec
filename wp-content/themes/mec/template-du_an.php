<?php
/**
 * Template Name: Dự Án
 * 
 */
$object_current = get_queried_object();

$paged = ($_GET['trang'] >= 2) ? $_GET['trang'] : 1;

$arg = [
	'post_type' => 'duan',
	'paged' => $paged,
];

if ($arg['post_type'] == 'duan' && is_page('du-an')) {
  add_action('pre_get_posts', function ($query) {
    $query->set('posts_per_page', 9);
  });
}

$du_an = new WP_Query($arg);


$data = [
	'du_an' => $du_an,
	'object_current' => $object_current,
];

view('template-du_an', $data);