<?php
/**
 * Template Name: News
 * 
 */

$arg = [
	'post_type' => 'news',
	'posts_per_page' => -1,
];

$highlights = new WP_Query($arg);


$data = [
	'highlights' => $highlights
];

view('news', $data);