@if($child_terms)
	<div class="list-item container homepage">
		<div class="row">
		@php
			foreach ($child_terms as $value) {

				$url = get_category_link($value->term_id);

				$thumbnail = get_field('thumbnail', 'wpdmcategory_' . $value->term_id);

				if (empty($thumbnail['url'])) {
				 $thumbnail['url'] = get_stylesheet_directory_uri().'/resources/assets/images/home/item-home-md.png';
				}
		@endphp
				<div class="col-md-4 col-sm-4 col-xs-12" style="margin-bottom:30px;">
					<div class="item-home">
						<a href="{{ $url }}">
							<img class="img-fluid" style="background: url({{ $thumbnail['url'] }}) no-repeat;" src="{{ get_stylesheet_directory_uri() }}/resources/assets/images/home/home-images.png">
						</a>
					</div>
				</div>
		@php
			}
		@endphp
		</div>
	</div>
@else
	<div class="list-item container">
		<div class="row">
		@php

			if(!empty($get_post_categoris->posts)){

			foreach ($get_post_categoris->posts as $key => $value) {

				$link = get_permalink($value->ID);
				$thumbnail = wp_get_attachment_url(get_post_thumbnail_id($value->ID));

				if(empty($thumbnail)){
					$thumbnail = get_stylesheet_directory_uri().'/resources/assets/images/home/default.png';
				}
		@endphp
				<div class="item col-md-3">
					<div class="content">
						<div class="info-item">
							<a href="{{ $link }}">
								<img style="background: url({{ $thumbnail }}) no-repeat;" src="{{ get_stylesheet_directory_uri() }}/resources/assets/images/home/images-product.png">
							</a>
						</div>
					</div>
					<div class="title-item">
						<p class="">
							<a href="{{ $link }}">
								{{ $value->post_title }}
							</a>
						</p>
					</div>
				</div>
		@php
			}
			}
		@endphp

			<div class="paginate">
				@php

				$total_pages = $get_post_categoris->max_num_pages;

				if ($total_pages > 1) :

				    $current_page = max(1, $paged);

				    echo paginate_links(array(
					  'base' => @add_query_arg('trang','%#%'),
					  'format' => '?trang=%#%',
					  'current' => $current_page,
					  'total' => $total_pages,
					        'prev_text'    => __('<'),
					        'next_text'    => __('>')
					));
				@endphp
				@php
				endif;
				@endphp
				@php wp_reset_postdata(); @endphp
	        </div>
		</div>
	</div>
@endif

