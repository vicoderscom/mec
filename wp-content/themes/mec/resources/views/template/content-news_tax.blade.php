<div class="tax_news">
	<div class="container">
		<div class="title_tax">
			<p>{{ $object_current->name }}</p>
		</div>

		<div class="list-item">
			<div class="row">
				@php
					while ($get_post_categoris->have_posts()): $get_post_categoris->the_post();

						$link = get_permalink(get_the_ID());
						$thumbnail = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));

						if(empty($thumbnail)){
							$thumbnail = get_stylesheet_directory_uri().'/resources/assets/images/home/default.png';
						}
				@endphp

				<div class="item_news col-md-4">
                    <a href="{{ $link }}">
                        <div class="images">
                          <img style="background: url({{ $thumbnail }}) no-repeat;" src="{{ get_stylesheet_directory_uri() }}/resources/assets/images/home/news.png">
                        </div>
                        <p class="title">{{ get_the_title(get_the_ID()) }}</p>
                        <p class="date"><i class="fa fa-clock-o" aria-hidden="true"></i> Ngày {{ get_the_date('d-m-Y', get_the_ID()) }}</p>

                        <p class="excerpt">{{ get_the_excerpt(get_the_ID()) }}</p>
                    </a>
                </div>

                @php
					endwhile;
				@endphp

				<div class="paginate">
					@php

					$total_pages = $get_post_categoris->max_num_pages;

					if ($total_pages > 1) :

					    $current_page = max(1, $paged);

					    echo paginate_links(array(
					  'base' => @add_query_arg('trang','%#%'),
					  'format' => '?trang=%#%',
					  'current' => $current_page,
					  'total' => $total_pages,
					        'prev_text'    => __('<'),
					        'next_text'    => __('>')
					));
					@endphp
					@php
					endif;
					@endphp
					@php wp_reset_postdata(); @endphp
		        </div>
			</div>
		</div>
	</div>
</div>