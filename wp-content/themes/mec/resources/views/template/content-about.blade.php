<div class="about">
	<div class="container">
		<div class="row content-about">
			@php
				while ( have_posts() ) : the_post();
						the_content();
				endwhile;
	    		wp_reset_query();
			@endphp
		</div>
	</div>
</div>
