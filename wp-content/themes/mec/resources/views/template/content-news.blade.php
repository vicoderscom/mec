<div class="list_news container">
    <div class="news-highlights">
        <div class="title">
            <p>Tin nổi bật</p>
        </div>

        <div class="item-highlights">
            @php
                while ($highlights->have_posts()): $highlights->the_post();

                $field_noibat =  get_field( 'highlights' , get_the_ID() );

                $thumbnail = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));

                if (empty($thumbnail)) {
                    $thumbnail = get_stylesheet_directory_uri().'/resources/assets/images/home/news-macdinh.png';
                }

                if($field_noibat[0] == 'co'){
            @endphp

              <div class="row">
                  <a href="{{ the_permalink() }}">
                    <div class="images-item col-md-4">
                        <img style="background: url({{ $thumbnail }}) no-repeat;" src="{{ get_stylesheet_directory_uri() }}/resources/assets/images/home/news.png">
                    </div>

                    <div class="info-item col-md-8">
                        <p class="title-item">{{ the_title() }}</p>
                        <p class="date"><i class="fa fa-clock-o" aria-hidden="true"></i> Ngày {{ get_the_date('d-m-Y') }}</p>
                        <p class="description">{!! get_the_excerpt() !!}</p>
                    </div>
                  </a>
              </div>

            @php
                  }
                endwhile;
            @endphp
        </div>
    </div>

    <div class="news_mec">
        <ul class="nav" id="myTab" role="tablist">
            @php

                $slug = explode('/', $_SERVER['HTTP_REFERER']);

                $slug[4];

              $args_cate = [
                'hierarchical'     => 1,
                'show_option_none' => '',
                'hide_empty'       => 0,
                'parent'           => 0,
                'taxonomy'         => 'news_tax',
              ];
              $news_categories = get_categories($args_cate);

              foreach ($news_categories as $key => $value) {
            @endphp
              <li class="nav-item">
                <a class="nav-link @php echo ($key === 0) ? 'active' : ''; @endphp" id="{{ $value->term_id }}-tab" data-toggle="tab" href="#{{ $value->term_id }}" role="tab" aria-controls="{{ $value->term_id }}" aria-expanded="true">{{ $value->name }}</a>
              </li>
            @php
              }
            @endphp
        </ul>

        <div class="row">
            <div class="tab-content">
              @php
                foreach ($news_categories as $key => $value2) {
              @endphp
                <div class="tab-pane fade show @php echo ($key === 0) ? 'active' : ''; @endphp" id="{{ $value2->term_id }}" role="tabpanel" aria-labelledby="{{ $value2->term_id }}-tab">
                    @php
                      $paged = ($_GET['trang'] >= 2) ? $_GET['trang'] : 1;

                      $args = [
                          'post_type'      => 'news',
                          'post_status' => 'publish',
                          'tax_query' => array(
                            array(
                              'taxonomy' => 'news_tax',
                              'field'    => 'id',
                              'terms'    => $value2->term_id,
                            ),
                          ),
                          'paged' => $paged,
                        ];
                        
                        if ($args['post_type'] == 'news' && is_page('tin-tuc')) {
                          add_action('pre_get_posts', function ($query) {
                            $query->set('posts_per_page', 6);
                          });
                        }

                      $loop_news = new WP_Query($args);

                      if ($loop_news->have_posts()) {
                        while ($loop_news->have_posts()): $loop_news->the_post();

                          $thumbnail = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));

                          if (empty($thumbnail)) {
                              $thumbnail = get_stylesheet_directory_uri().'/resources/assets/images/home/news-macdinh.png';
                          }
                    @endphp

                    <div class="item_news col-md-4">
                        <a href="{{ the_permalink() }}">
                            <div class="images">
                              <img style="background: url({{ $thumbnail }}) no-repeat;" src="{{ get_stylesheet_directory_uri() }}/resources/assets/images/home/news.png">
                            </div>
                            <p class="title">{{ the_title() }}</p>
                            <p class="date"><i class="fa fa-clock-o" aria-hidden="true"></i> Ngày {{ get_the_date('d-m-Y') }}</p>

                            <p class="excerpt">{!! get_the_excerpt() !!}</p>
                        </a>
                    </div>

                    @php
                        endwhile;
                      }else{
                        echo __('Tin Tức Đang Cập Nhập');
                      }
                    @endphp
                  <div class="paginate">
                      @php
                        
                        $total_pages = $loop_news->max_num_pages;

                        if ($total_pages > 1) :

                            $current_page = max(1, $paged);

                            echo paginate_links(array(
                          'base' => @add_query_arg('trang','%#%'),
                          'format' => '?trang=%#%',
                          'current' => $current_page,
                          'total' => $total_pages,
                                'prev_text'    => __('<'),
                                'next_text'    => __('>')
                      ));
                      @endphp
                      @php
                        endif;
                      @endphp
                      @php wp_reset_postdata(); @endphp
                  </div>
                </div>


              @php
                }
              @endphp
            </div>
          </div>
    </div>
</div>

