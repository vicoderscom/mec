<div class="slider-home">
	@php
		echo do_shortcode('[rev_slider alias="home"]');
	@endphp
</div>

<div id="search_home" class="home-input">
    <div class="container">
        <div class="col-xs-12 search_header">

            @php
                get_search_form();
            @endphp
           
        </div>
    </div>
</div>

<div class="homepage">
	<div class="container">
		<div class="row">
			@php

				foreach ($categories as $value) {

					$url = get_category_link($value->term_id);

					$thumbnail = get_field('thumbnail', 'wpdmcategory_' . $value->term_id);

					if (empty($thumbnail['url'])) {
					 $thumbnail['url'] = get_stylesheet_directory_uri().'/resources/assets/images/home/item-home-md.png';
					}
			@endphp

				<div class="col-md-4 col-sm-4 col-xs-12" style="margin-bottom:30px;">
					<div class="item-home">
						<a href="{{ $url }}">
							<img class="img-fluid" style="background: url({{ $thumbnail['url'] }}) no-repeat;" src="{{ get_stylesheet_directory_uri() }}/resources/assets/images/home/home-images.png">
						</a>
					</div>
				</div>

			@php
				}
			@endphp
		</div>
		@include('advert.advert')
	</div>
</div>