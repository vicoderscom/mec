<div class="breadcrumbs">
	<div class="container">
		<a href="http://mec.dev">Trang chủ </a><i class="fa fa-chevron-right" aria-hidden="true"></i>
		<span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to the Tin tức." href="{{ site_url('tin-tuc') }}" class="taxonomy wpdmcategory"><span property="name">Tin tức</span></a><meta property="position" content="1"></span><i class="fa fa-chevron-right" aria-hidden="true"></i>

		<span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to the Tin tức." href="{{ site_url('news_tax') }}/{{ $cat_current_post[0]->slug }}" class="taxonomy wpdmcategory"><span property="name">{{ $cat_current_post[0]->name }}</span></a><meta property="position" content="1"></span><i class="fa fa-chevron-right" aria-hidden="true"></i>

		<span property="itemListElement" typeof="ListItem"><span property="name">{{ $object_current->post_title }}</span><meta property="position" content="2"></span>                    
	</div>
</div>

{{-- //////////////////////////////////////////////// --}}

<div class="single-news container">
	<div class="news">
		@php
			if (have_posts()){

				while (have_posts()) :
				the_post();
		@endphp

			<div class="title_date">
				<p class="title_post">{{ the_title() }}</p>
				<p class="date"><i class="fa fa-clock-o" aria-hidden="true"></i> Ngày {{ get_the_date('d-m-Y') }}</p>
			</div>

			<div class="content">
				{!! the_content() !!}
			</div>

		@php
				endwhile;
			}
		@endphp
	</div>

	<div class="news-lienquan">
		<div class="title_name">
        	<p>Tin liên quan</p>
    	</div>

    	<div class="row">
    		@php
    			while ($related->have_posts()): $related->the_post();

    				$thumbnail = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
    		@endphp
				<div class="item_news col-md-4">
	                <a href="{{ the_permalink() }}">
	                    <div class="images">
	                      <img style="background: url({{ $thumbnail }}) no-repeat;" src="{{ get_stylesheet_directory_uri() }}/resources/assets/images/home/news.png">
	                    </div>
	                    <p class="title">{{ the_title() }}</p>
	                    <p class="date"><i class="fa fa-clock-o" aria-hidden="true"></i> Ngày {{ get_the_date('d-m-Y') }}</p>

	                    <p class="excerpt">{!! get_the_excerpt() !!}</p>
	                </a>
	            </div>
	        @php
	        	endwhile;
	        @endphp
    	</div>
	</div>
</div>