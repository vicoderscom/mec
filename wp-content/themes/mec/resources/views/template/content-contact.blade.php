<div class="container contact_page">
	<div class="row">
		@php
			while ( have_posts() ) : the_post();
					the_content();
			endwhile;
    		wp_reset_query();
		@endphp
	</div>
</div>