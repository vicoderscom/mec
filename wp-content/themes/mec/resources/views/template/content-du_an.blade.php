<div class="du_an">
	<div class="container">
		
		<div class="title_duan">
			<p>{{ $object_current->post_title }}</p>
		</div>

		<div class="row">
			@php
				while ($du_an->have_posts()): $du_an->the_post();

				$thumbnail = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));

                if (empty($thumbnail)) {
                    $thumbnail = get_stylesheet_directory_uri().'/resources/assets/images/home/news-macdinh.png';
                }

			@endphp

				<div class="item_news col-md-4">
	                <a href="{{ the_permalink() }}">
	                    <div class="images">
	                      <img style="background: url({{ $thumbnail }}) no-repeat;" src="{{ get_stylesheet_directory_uri() }}/resources/assets/images/home/news.png">
	                    </div>
	                    <p class="title">{{ the_title() }}</p>
	                    <p class="date"><i class="fa fa-clock-o" aria-hidden="true"></i> Ngày {{ get_the_date('d-m-Y') }}</p>

	                    <p class="excerpt">{!! get_the_excerpt() !!}</p>
	                </a>
	            </div>

            @php
                endwhile;
            @endphp

            <div class="paginate">
              @php
                
                $total_pages = $du_an->max_num_pages;

                if ($total_pages > 1) :

                    $current_page = max(1, $paged);

                    echo paginate_links(array(
                  'base' => @add_query_arg('trang','%#%'),
                  'format' => '?trang=%#%',
                  'current' => $current_page,
                  'total' => $total_pages,
                        'prev_text'    => __('<'),
                        'next_text'    => __('>')
              ));
              @endphp
              @php
                endif;
              @endphp
              @php wp_reset_postdata(); @endphp
          </div>
		</div>	
	</div>
</div>