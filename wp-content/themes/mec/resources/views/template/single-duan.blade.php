<div class="breadcrumbs">
<div class="container">
	<a href="http://mec.dev">Trang chủ </a><i class="fa fa-chevron-right" aria-hidden="true"></i>

	<span property="itemListElement" typeof="ListItem"><a property="item" typeof="WebPage" title="Go to Dự án." href="{{ site_url('du-an') }}" class="post post-duan-archive"><span property="name">Dự án</span></a><meta property="position" content="1"></span><i class="fa fa-chevron-right"></i>

	<span property="itemListElement" typeof="ListItem"><span property="name">{{ $object_current->post_title }}</span><meta property="position" content="2"></span>                    
</div>
</div>

<div class="single_duan">
	<div class="container">
		<div class="news">
			@php
				if (have_posts()){

					while (have_posts()) :
					the_post();
			@endphp

				<div class="title_date">
					<p class="title_post">{{ the_title() }}</p>
					<p class="date"><i class="fa fa-clock-o" aria-hidden="true"></i> Ngày {{ get_the_date('d-m-Y') }}</p>
				</div>

				<div class="content">
					{!! the_content() !!}
				</div>

			@php
					endwhile;
				}
			@endphp
		</div>
	</div>
</div>