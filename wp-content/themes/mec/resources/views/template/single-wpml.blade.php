<div class="single-wpml container">
	<div class="view">
		@php

			while (have_posts()) :
				the_post();
				if ($content[0] === 'co'){

				$field_noidung = get_field('noi_dung_text', get_the_ID());

				$thongtin_product = get_field('thong_tin_san_pham', get_the_ID());

				$product_gallery = get_field('product_gallery', get_the_ID());

				$thumbnail = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));

		@endphp

			<div class="single_noidung">
				<div class="row">
					<div class="col-md-6 info_product">
						<div id="page">
						    <div class="container">
							    <div class="slider-for">
							    	<div class="images_daidien">
								    	<img style="background: url({{ $thumbnail }}) no-repeat;" src="{{ get_stylesheet_directory_uri() }}/resources/assets/images/home/images_detail.png">
							    	</div>

								    @php
										foreach ($product_gallery as $value) {
									@endphp
										<div class="images_daidien">
											<img style="background: url({{ $value['url'] }}) no-repeat;" src="{{ get_stylesheet_directory_uri() }}/resources/assets/images/home/images_detail.png">
										</div>
									@php
										}
									@endphp
							    </div>
							    <div class="slider-nav">
							    	<div class="gallery">
								    	<img style="background: url({{ $thumbnail }}) no-repeat;" src="{{ get_stylesheet_directory_uri() }}/resources/assets/images/home/gallery.png">
								    </div>
								    @php
										foreach ($product_gallery as $value) {
									@endphp
										<div class="gallery">
											<img style="background: url({{ $value['url'] }}) no-repeat;" src="{{ get_stylesheet_directory_uri() }}/resources/assets/images/home/gallery.png">
										</div>
									@php
										}
									@endphp
							    </div>
						    </div>
						</div>
					</div>

					<div class="col-md-6 thongso_product">
						<p class="title_post">{{ the_title() }}</p>
						<div class="detail_product">
							{!! $thongtin_product !!}
						</div>
					</div>
				</div>

				<div class="mota_product">
					<div class="mota">
						<p class="title_mota">Mô tả sản phẩm</p>
					</div>

					<div class="content_mota">
						{{ the_content() }}
					</div>
				</div>
			</div>

			<div class="product_lienquan list-item container">
				<div class="product_lq">
					<p>sản phẩm liên quan</p>
				</div>

				<div class="row">
					@php

						$cat_current = get_the_terms($object_current->ID, 'wpdmcategory');

						$post_cat = [
						    'post_type'      => 'wpdmpro',
						    'posts_per_page' => 4,
						    'post_status'    => 'publish',
						    'tax_query'      => [
						        [
						            'taxonomy'         => 'wpdmcategory',
						            'terms'            => $cat_current[0]->term_id,
						            'field'            => 'term_id',
						            'operator'         => 'AND',
						            'include_children' => false,
						        ],
						    ],
						];

						$get_post_categoris = new WP_Query($post_cat);

						foreach ($get_post_categoris->posts as $value) {

							if($value->ID === $object_current->ID){
								continue;
							}

							$thumbnail = wp_get_attachment_url(get_post_thumbnail_id($value->ID));

							$link = get_the_permalink($value->ID);
					@endphp

						<div class="item col-md-3">
							<div class="content">
								<div class="info-item">
									<a href="{{ $link }}">
										<img style="background: url({{ $thumbnail }}) no-repeat;" src="{{ get_stylesheet_directory_uri() }}/resources/assets/images/home/images-product.png">
									</a>
								</div>
							</div>
							<div class="title-item">
								<p class="">
									<a href="{{ $link }}">
										{{ $value->post_title }}
									</a>
								</p>
							</div>
						</div>

					@php
						}
					@endphp
				</div>
			</div>

		@php
			}else{
				$url_preview = getLinkForPreview(get_the_ID());
		@endphp

	            <iframe src="https://docs.google.com/viewer?url=<?php echo urlencode($url_preview); ?>&embedded=true" width="100%" height="600" style="border: none;"></iframe>

	            <p class="thongbao">Click download để tải tài liệu</p>

	            <div class="download">
	            	<div class="click_download">
	            		<a href="{{ $url_preview }}">
	            			<i class="fa fa-cloud-download" aria-hidden="true"></i> Download
	            		</a>
	            	</div>
	            </div>

	    @php
			}
			endwhile;
		@endphp

	</div>
</div>