@extends('layouts.app')

@section('content')
    @include('partials.page-header')

    @include('template.content-about')
@endsection
