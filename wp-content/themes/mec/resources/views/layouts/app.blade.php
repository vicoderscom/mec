<!DOCTYPE html>
<html {!! language_attributes() !!}>

  @include('partials.head')

    <body {!! body_class() !!}>

    {!! do_action('get_header') !!}

    @include('partials.header')
    <div class="mec">
        <div class="wrap" role="document">
          <div class="content">
            @if(!is_home() && !is_front_page() && !is_singular('news') && !is_singular('duan'))
                <div class="breadcrumbs">
                    <div class="container">
                        @php
                        if(function_exists('bcn_display')) {
                            echo '<a href="' . site_url() . '">Trang chủ </a><i class="fa fa-chevron-right" aria-hidden="true"></i> ';
                            bcn_display(); 
                        } 
                        @endphp
                    </div>
                </div>
            @endif
            <main class="main">
                @yield('content')
            </main>
          </div>
        </div>
    </div>
    <div class="adver">
        @yield('adver')
    </div>

    {!! do_action('get_footer') !!}

    @include('partials.footer')

    {!! wp_footer() !!}

    </body>
</html>
