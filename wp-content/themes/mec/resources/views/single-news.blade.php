@extends('layouts.app')

@section('content')
    @include('partials.page-header')

    @include('template.single-news')
@endsection
