<div class="slider-quangcao">		
	<div class="row">
		@php
			$thumbnail_slider = get_field('quang_cao');

			foreach ($thumbnail_slider as $value) {

				if(empty($value['url'])){
					$value['url'] = get_stylesheet_directory_uri().'/resources/assets/images/home/slider.png';
				}

		@endphp
			<div class="col-md-12">
			    {{-- <img style="background: url({{ $value['url'] }}) no-repeat;" src="{{ get_stylesheet_directory_uri() }}/resources/assets/images/home/slick-slider.png"/> --}}
			    <img class="img-fluid" src="{{ $value['url'] }}"/>
		    </div>
		@php
			};
		@endphp
	</div>
</div>