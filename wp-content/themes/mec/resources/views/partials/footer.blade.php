<div id="top-footer">
	<div class="icon-top">
		<i class="fa fa-chevron-up" aria-hidden="true"></i>
	</div>
</div>
<footer class="content-info">
    <div class="container">
        <div class="row">
        	<div class="info_footer col-md-5">
        		@php
					dynamic_sidebar('footer_diachi');
				@endphp
        	</div>

        	<div class="menu-footer col-md-2">
        		@php
					dynamic_sidebar('footer_about');
				@endphp
        	</div>

        	<div class="icon-shear-footer col-md-2">
        		@php
					dynamic_sidebar('footer_share');
				@endphp
        	</div>

        	<div class="thanhtoan col-md-3">
        		@php
					dynamic_sidebar('footer_thanhtoan');
				@endphp
        	</div>
        </div>

    </div>
    <div class="copy-right">
    	<p><i class="fa fa-copyright" aria-hidden="true"></i> 2017 - Bản quyền thuộc về Công ty cổ phần MEC	Việt Nam</p>
    </div>
</footer>
{{-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> --}}
