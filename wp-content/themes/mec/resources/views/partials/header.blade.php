<header class="banner">
  <div class="container header-mec">
    <!-- <a class="brand" href="{{ home_url('/') }}">
        {{ get_bloginfo('name', 'display') }}
    </a> -->
    <div class="row">
        <div class="item-header logo col-md-2">
            @php
                $customLogoId = get_theme_mod('custom_logo');
                $logo = wp_get_attachment_image_src($customLogoId , 'full');
            @endphp
            <a href="{!! site_url() !!}">
                <img src="{{ $logo[0] }}">
            </a>
        </div>

        @php
            dynamic_sidebar('header');
        @endphp
    </div>
  </div>
</header>

<div class="menu">
    <div class="container">
        <nav class="nav-primary">
            @if (has_nav_menu('main-menu'))
            {!! wp_nav_menu(['theme_location' => 'main-menu', 'menu_class' => 'nav']) !!}
            @endif
        </nav>
    </div>
    <div class="mobile-menu"></div>
</div>

{{-- <div class="call">
    <i class="fa fa-phone" aria-hidden="true"></i>
</div>

<div class="list_call">
    @php
        dynamic_sidebar('header');
    @endphp
</div> --}}

<div class="call" data-toggle="modal" data-target="#flipFlop">
    <i class="fa fa-phone" aria-hidden="true"></i>
</div>


<div class="modal fade" id="flipFlop" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title" id="modalLabel"></h4>
            </div>

            <div class="modal-body list_call">
                @php
                    dynamic_sidebar('header');
                @endphp
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>