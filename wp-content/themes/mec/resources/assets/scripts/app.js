import 'jquery';
import 'bootstrap';
import 'slick-carousel/slick/slick.min.js';
import 'jquery.meanmenu/jquery.meanmenu.js';

$(document).ready(function () {
	// console.log('ok');
	// new WOW().init();
    $('.nav-primary .menu-main-menu-container').meanmenu({
        meanScreenWidth: "768",
        meanMenuContainer: ".mobile-menu",
    });

	$('.homepage .slider-quangcao .row').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        autoplay: true,
        pauseOnHover: true,
        responsive: [{
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
            }
        }]        
    });

    $('.news-highlights .item-highlights').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        pauseOnHover: true,
        responsive: [{
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
            }
        }]        
    });

    //news

    $('.item-highlights .slick-prev').click(function(){
        $(this).addClass('style-icon-prev');

        var next = $('.item-highlights .slick-next').hasClass('style-icon-next');

        // console.log(next);

        if(next === true){
            $('.item-highlights .slick-next').removeClass('style-icon-next');
        }
    });

    $('.item-highlights .slick-next').click(function(){
        $(this).addClass('style-icon-next');

        var prev = $('.item-highlights .slick-prev').hasClass('style-icon-prev');

        if(prev === true){
            $('.item-highlights .slick-prev').removeClass('style-icon-prev');
        }
    });

    //fix tabs bootraps 4
    $('#myTab a').click(function (e) {
      e.preventDefault();
      $('.nav-link').removeClass('active');
      $(this).tab('show');
    });

    $('.nav-primary .menu-main-menu-container ul li').hover(function() {
        $(this).find('ul:first').show(400).css({ 'visibility': 'visible' });
    }, function() {
        $(this).find('ul:first').css({ 'display': 'none' });
    });

    $("#top-footer .icon-top").on('click', function() {
        $('html, body').animate({
            scrollTop: $('html, body').offset().top
        }, 1000);
    });

    $('.single-news img').addClass('img-fluid');

    $('.single_duan img').addClass('img-fluid');

    $('.single_noidung .mota_product .content_mota img').addClass('img-fluid');

    $('.slider-for').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      asNavFor: '.slider-nav'
    });

    $('.slider-nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        focusOnSelect: true,
        arrows: true,
    });

});

