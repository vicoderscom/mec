<?php
/**
 * Template Name: Home Page
 * 
 */

$params = [
    'hierarchical'     => 1,
    'show_option_none' => '',
    'hide_empty'       => 0,
    'parent'           => 0,
    'taxonomy'         => 'wpdmcategory',
];

$categories = get_categories($params);

view('home', ['categories' => $categories]);