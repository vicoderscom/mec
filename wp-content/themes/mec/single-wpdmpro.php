<?php

$object_current = get_queried_object();

$cat_current_post = get_the_terms($object_current->ID, 'wpdmcategory');

$content = get_field('noi_dung', 'wpdmcategory_' . $cat_current_post[0]->term_id);

$data = [
	'content' => $content,
	'object_current' => $object_current,
];

view('single-wpml', $data);