<?php

namespace App\Widgets;

use MSC\Widget;

/**
 *
 */
class ContactHeader extends Widget
{
    public function __construct()
    {
        $widget = [
            'id'          => __('header_widget', 'textdomain'),
            'label'       => __('Contact Header Widget', 'textdomain'),
            'description' => __('This is a sample widget', 'textdomain'),
        ];

        $fields = [

            [
                'label' => __('Điền Email', 'textdomain'),
                'name'  => 'email_2',
                'type'  => 'text',
            ],

            [
                'label' => __('Email', 'textdomain'),
                'name'  => 'email',
                'type'  => 'text',
            ],

            [
                'label' => __('SĐT Liên hệ 3', 'textdomain'),
                'name'  => 'sdt_3',
                'type'  => 'text',
            ],

            [
                'label' => __('Liên hệ 3', 'textdomain'),
                'name'  => 'name_3',
                'type'  => 'text',
            ],

            [
                'label' => __('SĐT Liên hệ 2', 'textdomain'),
                'name'  => 'sdt_2',
                'type'  => 'text',
            ],

            [
                'label' => __('Liên hệ 2', 'textdomain'),
                'name'  => 'name_2',
                'type'  => 'text',
            ],

            [
                'label' => __('SĐT Liên hệ 1', 'textdomain'),
                'name'  => 'sdt_1',
                'type'  => 'text',
            ],

            [
                'label' => __('Liên hệ 1', 'textdomain'),
                'name'  => 'name_1',
                'type'  => 'text',
            ],


        ];
        parent::__construct($widget, $fields);
    }

    public function handle($instance)
    {
        ?>

        <div class="item-header col-md-3">
            <div class="icon-phone"><i class="fa fa-phone" aria-hidden="true"></i></div>
            <div class="phone-header">
                <p class="title"><?php echo $instance['name_1']; ?></p>
                <p class="number-phone"><?php echo $instance['sdt_1']; ?></p>
            </div>
        </div>

        <div class="item-header col-md-3">
            <div class="icon-phone"><i class="fa fa-phone" aria-hidden="true"></i></div>
            <div class="phone-header">
                <p class="title"><?php echo $instance['name_2']; ?></p>
                <p class="number-phone"><?php echo $instance['sdt_2']; ?></p>
            </div>
        </div>

        <div class="item-header col-md-3">
            <div class="icon-phone"><i class="fa fa-phone" aria-hidden="true"></i></div>
            <div class="phone-header">
                <p class="title"><?php echo $instance['name_3']; ?></p>
                <p class="number-phone"><?php echo $instance['sdt_3']; ?></p>
            </div>
        </div>

        <div class="item-header col-md-3">
            <div class="icon-phone"><i class="fa fa-envelope-o" aria-hidden="true"></i></div>
            <div class="phone-header">
                <p class="title"><?php echo $instance['email']; ?></p>
                <p class="number-phone"><?php echo $instance['email_2']; ?></p>
            </div>
        </div>

        <?php
	}
}
