<?php

namespace App\Taxonomies;

use MSC\Tax;

class News extends Tax
{
	public function __construct()
	{
		$config = [
			'slug' => 'news_tax',
			'single' => 'News Category',
			'plural' => 'News Category'
		];

		$postType = 'news';

		$args = [

		];

		parent::__construct($config, $postType, $args);
	}
}