<?php
$object_current = get_queried_object();

$paged = ($_GET['trang'] >= 2) ? $_GET['trang'] : 1;

$post_cat = [
	'post_type'=>'news',
    'post_status'    => 'publish',
    'tax_query'     => array(
        array(
            'taxonomy'          => 'news_tax',
            'terms'             => array($object_current->term_id),
            'field'             => 'term_id',
        )
    ),

    'paged' => $paged,
];

add_action('pre_get_posts', function ($query) {
	$query->set('posts_per_page', 9);
});

$get_post_categoris = new WP_Query($post_cat);

$data = [
	'object_current' => $object_current,
	'get_post_categoris' => $get_post_categoris
];

view('taxonomy-news_tax', $data);