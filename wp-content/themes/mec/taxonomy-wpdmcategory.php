<?php
$object_current = get_queried_object();

$child_terms = get_terms([
    'taxonomy'   => 'wpdmcategory',
    'hide_empty' => false,
    'parent'     => $object_current->term_id,
]);


$paged = ($_GET['trang'] >= 2) ? $_GET['trang'] : 1;

$post_cat = [
	'post_type'=>'wpdmpro',
    'post_status'    => 'publish',
    'tax_query'     => array(
        array(
            'taxonomy'          => 'wpdmcategory',
            'terms'             => array($object_current->term_id),
            'field'             => 'term_id',
        )
    ),

    'paged' => $paged,
];

add_action('pre_get_posts', function ($query) {
	$query->set('posts_per_page', 9);
});

$get_post_categoris = new WP_Query($post_cat);

$data = [
	'get_post_categoris' => $get_post_categoris,
    'child_terms' => $child_terms
];
view('taxonomy-wpdmcategory', $data);