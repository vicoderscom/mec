<?php

$object_current = get_queried_object();
$cat_current_post = get_the_terms($object_current->ID, 'news_tax');
// echo "<pre>";
// var_dump($cat_current_post[0]->name);

$arg = [
	'post_type' => 'news',
	'orderby' => 'rand'
];

if ($arg['post_type'] == 'news' && $arg['orderby'] == 'rand') {
    add_action('pre_get_posts', function ($query) {
        $query->set('posts_per_page', 3);
    });
}

$related = new WP_Query($arg);

$data = [
	'related' => $related,
	'object_current' => $object_current,
	'cat_current_post' => $cat_current_post,
];
view('single-news', $data);